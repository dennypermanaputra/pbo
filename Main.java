package mahasiswa;

import java.util.Scanner;

public class Main {
	
	public static void main(String[]args){
        
		try (Scanner scan1 = new Scanner(System.in)) {
			System.out.print("Masukkan Nim: ");
			String nim = scan1.nextLine();
			System.out.print("Masukkan Nama: ");
			String Nama = scan1.nextLine();
			System.out.print("Masukkan Ipk: ");
			double Ipk = scan1.nextDouble();
			System.out.print("Masukkan SKS: ");
			int Sks = scan1.nextInt();
			System.out.print("Masukkan Tanggal Lahir: ");
			String Tgllhr = scan1.next();
			
			
		
			
			Mahasiswa mahasiswa = new Mahasiswa(nim, Nama, Ipk, Sks, Tgllhr);
			/**
			 * A11.2020.12925
			 * nim.substring(0,3) // A11
			 * nim.substring(4,8) // 2020
			 * nim.substring(9,14) // 12925
			 */
			String prodi1 = nim.substring(0,3);
			System.out.println("\n\n=============================================================================\n\n");
			System.out.println("Umur Mahasiswa				: "+Nama);
			System.out.println("Program Studi Mahasiswa 	: "+mahasiswa.getProgdi(prodi1));
			System.out.println("Status Ipk Mahasiswa 		: "+mahasiswa.ipkStatus());
			System.out.println("Tahun Angkatan Mahasiswa	: "+mahasiswa.getTahun());
			System.out.println("Tagihan SKS Mahasiswa		: Rp."+mahasiswa.getTagihanSks());
			System.out.println("Semester Mahasiswa			: Semester "+mahasiswa.getMhsSemester());
			System.out.println("Umur Mahasiswa				: ");
			String For = "yyyy-mm-dd";
			mahasiswa.DISPLAY1(For, Tgllhr);
			System.out.println("Umur Mahasiswa				: "+ mahasiswa.getUmur());
			
			
			
			
		}
		
	}

}
